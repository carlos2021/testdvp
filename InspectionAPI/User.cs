﻿using System.ComponentModel.DataAnnotations;

namespace InspectionAPI
{
    public class User
    {
        public int Id { get; set; }

        [Display(Name = "Usuario")]
        [Required(ErrorMessage = "digite su usuario")]
        [StringLength(100, ErrorMessage = "error", MinimumLength = 2)]
        public string? Username { get; set; }

        [Display(Name = "contraseña")]
        [StringLength(100, ErrorMessage = "error", MinimumLength = 2)]
        [Required(ErrorMessage = "digite su contraseña")]
        public string? Pass { get; set; }
        public DateTime DateCreated { get; set; }
        public Person? Person { get; set; }
    }
}
