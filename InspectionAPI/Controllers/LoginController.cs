﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InspectionAPI;
using InspectionAPI.Data;

namespace InspectionAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly DataContext _context;

        public LoginController(DataContext context)
        {
            _context = context;
        }
        
        // POST: api/login
        [HttpPost("{user},{password}")]
        public async Task<ActionResult<User>> PostInspection(string user, string password, User userObject)
        {
            if (user =="carlos, faltaaa")
                return CreatedAtAction("change", new { id = userObject.Id }, userObject);
            return CreatedAtAction("change it also", new { id = userObject.Id }, userObject);
        }

    }
}
