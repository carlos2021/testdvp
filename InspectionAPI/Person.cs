﻿using System.ComponentModel.DataAnnotations;

namespace InspectionAPI
{
    public class Person
    {
        public int Id { get; set; }

        [Display(Name = "Nombres")]
        [Required(ErrorMessage = "digite su nombre")]
        [StringLength(100, ErrorMessage = "error", MinimumLength = 2)]
        public string? FirstName { get; set; }

        [Display(Name = "apellidos")]
        [StringLength(100, ErrorMessage = "error", MinimumLength = 2)]
        [Required(ErrorMessage = "digite su apellido")]
        public string? LastName { get; set; }

        [Display(Name = "email")]
        [Required(ErrorMessage = "digite su email")]
        [StringLength(100, ErrorMessage = "error", MinimumLength = 2)]
        public string? Email { get; set; }

        [Display(Name = "numero de identificación")]
        [Required(ErrorMessage = "digite su numero de identificacion")]
        public int NumberId { get; set; }

        [Display(Name = "tipo de identificacion")]
        [Required(ErrorMessage = "digite su tipo de identificación")]
        [StringLength(100, ErrorMessage = "error", MinimumLength = 2)]
        public string? TypeId { get; set; }

        public DateTime DateCreated { get; set; }


    }
}
